
""""""""""""""""""""""""""""""
"__   _(_)_ __ ___  _ __ ___ "
"\ \ / / | '_ ` _ \| '__/ __|"
" \ V /| | | | | | | | | (__ "
"  \_/ |_|_| |_| |_|_|  \___|"
""""""""""""""""""""""""""""""                            
set number
set relativenumber
syntax on
set tabstop=4

color theme

" JUMPER
inoremap <Space><Space> <Esc>/<++><CR>3xs

" LATEX FILE TYPE
autocmd BufNewFile,BufRead "*.tex" set filetype=tex
